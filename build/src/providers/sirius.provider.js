"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var tsjs_xpx_chain_sdk_1 = require("tsjs-xpx-chain-sdk");
var config = __importStar(require("../../config.json"));
var SiriusProvider = /** @class */ (function () {
    function SiriusProvider(listener) {
        this.listener = listener;
        this.networkType = tsjs_xpx_chain_sdk_1.NetworkType[config.networkType];
        this.transactionHttp = new tsjs_xpx_chain_sdk_1.TransactionHttp(config.httpNodeURL);
    }
    SiriusProvider.prototype.generateAccount = function () {
        return tsjs_xpx_chain_sdk_1.Account.generateNewAccount(this.networkType);
    };
    SiriusProvider.prototype.convertToMultiSignAccount = function (toConvert, minApproval, minRemoval, accounts) {
        return __awaiter(this, void 0, void 0, function () {
            var cosigners, convertIntoMultisigAccount, aggregateTransaction, signedTransaction, hashLockTransaction, hashLockTransactionSigned;
            var _this = this;
            return __generator(this, function (_a) {
                cosigners = accounts.map(function (cosigner) { return new tsjs_xpx_chain_sdk_1.MultisigCosignatoryModification(tsjs_xpx_chain_sdk_1.MultisigCosignatoryModificationType.Add, cosigner.publicAccount); });
                convertIntoMultisigAccount = tsjs_xpx_chain_sdk_1.ModifyMultisigAccountTransaction.create(tsjs_xpx_chain_sdk_1.Deadline.create(), minApproval, minRemoval, cosigners, this.networkType);
                aggregateTransaction = tsjs_xpx_chain_sdk_1.AggregateTransaction.createBonded(tsjs_xpx_chain_sdk_1.Deadline.create(), [convertIntoMultisigAccount.toAggregate(toConvert.publicAccount)], this.networkType);
                signedTransaction = aggregateTransaction.signWith(toConvert, config.generationHash);
                hashLockTransaction = tsjs_xpx_chain_sdk_1.HashLockTransaction.create(tsjs_xpx_chain_sdk_1.Deadline.create(), new tsjs_xpx_chain_sdk_1.Mosaic(new tsjs_xpx_chain_sdk_1.MosaicId('bffb42a19116bdf6'), tsjs_xpx_chain_sdk_1.UInt64.fromUint(0)), tsjs_xpx_chain_sdk_1.UInt64.fromUint(480), signedTransaction, this.networkType);
                hashLockTransactionSigned = hashLockTransaction.signWith(toConvert, config.generationHash);
                this.transactionHttp.announce(hashLockTransactionSigned).toPromise().then(function (response) {
                    console.log(response);
                }).catch(function (err) { return console.log(err); });
                console.log("58 - transactionHttp.announce(hashLockTransactionSigned) by " + toConvert.publicAccount.publicKey + " -> " + hashLockTransactionSigned.hash);
                return [2 /*return*/, this.createConfirmedListenerPromise(toConvert.address, '77', function (resolve, transaction, subscription) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            console.log("transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash -> ", transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash);
                            if (transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash) {
                                this.transactionHttp.announceAggregateBonded(signedTransaction).toPromise();
                                console.log("63 - this.transactionHttp.announceAggregateBonded(signedTransaction) -> " + signedTransaction.hash);
                                resolve(this.createPartialListenerPromise(toConvert.address, '83', function (partialResolve, partialTransaction, partialSubscription) { return __awaiter(_this, void 0, void 0, function () {
                                    var toSignCosignTransaction_1, cosignPromise;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (!(partialTransaction !== undefined && partialTransaction.transactionInfo.hash === signedTransaction.hash)) return [3 /*break*/, 2];
                                                console.log("68 - CosignatureTransaction.create(partialTransaction) -> " + partialTransaction.transactionInfo.hash);
                                                toSignCosignTransaction_1 = tsjs_xpx_chain_sdk_1.CosignatureTransaction.create(partialTransaction);
                                                return [4 /*yield*/, Promise.all(accounts.map(function (account) { return __awaiter(_this, void 0, void 0, function () {
                                                        var signedCosignedTransaction;
                                                        return __generator(this, function (_a) {
                                                            signedCosignedTransaction = account.signCosignatureTransaction(toSignCosignTransaction_1);
                                                            console.log("73 - this.transactionHttp.announceAggregateBondedCosignature(signedCosignedTransaction) -> " + account.publicAccount.publicKey + " -> " + partialTransaction.transactionInfo.hash);
                                                            this.transactionHttp.announceAggregateBondedCosignature(signedCosignedTransaction);
                                                            return [2 /*return*/, this.createConfirmedListenerPromise(account.address, '105', function (cosignResolve, cosignedTransaction, cosignSubscription) {
                                                                    console.log("76 - cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash ->", cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash);
                                                                    if (cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash) {
                                                                        console.log("78 -> cosignSubscription.unsubscribe() -> " + cosignedTransaction.transactionInfo.hash);
                                                                        cosignSubscription.unsubscribe();
                                                                        cosignResolve(true);
                                                                    }
                                                                })];
                                                        });
                                                    }); })).then(function (args) {
                                                        console.log(args);
                                                        console.log("84 - partialResolve(cosignPromise)");
                                                        partialSubscription.unsubscribe();
                                                        partialResolve();
                                                        subscription.unsubscribe();
                                                    }).catch(function () { return null; })];
                                            case 1:
                                                cosignPromise = _a.sent();
                                                _a.label = 2;
                                            case 2: return [2 /*return*/];
                                        }
                                    });
                                }); }));
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    SiriusProvider.prototype.createConfirmedListenerPromise = function (address, type, callBack) {
        var _this = this;
        return new Promise(function (resolve) {
            var subscription = _this.listener.confirmed(address).subscribe(function (transaction) {
                callBack(resolve, transaction, subscription);
            });
        }).catch(function () { return null; });
    };
    SiriusProvider.prototype.createPartialListenerPromise = function (address, type, callBack) {
        var _this = this;
        return new Promise(function (resolve) {
            var subscription = _this.listener.aggregateBondedAdded(address).subscribe(function (transaction) {
                callBack(resolve, transaction, subscription);
            });
        }).catch(function () { return null; });
    };
    SiriusProvider.prototype.generateSignerNode = function (provider, signerCount, minApproval, minRemoval) {
        return __awaiter(this, void 0, void 0, function () {
            var signerParents;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        signerParents = new Array(signerCount).fill(null).map(function (item) { return _this.generateAccount(); });
                        return [4 /*yield*/, this.convertToMultiSignAccount(provider, minApproval, minRemoval, signerParents).catch(function () { return null; })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, Promise.resolve({ provider: provider, signerParents: signerParents })];
                }
            });
        });
    };
    SiriusProvider.prototype.convertToKeys = function (account) {
        return {
            address: account.address.plain(),
            publicKey: account.publicAccount.publicKey,
            privateKey: account.privateKey,
        };
    };
    return SiriusProvider;
}());
exports.SiriusProvider = SiriusProvider;
//# sourceMappingURL=sirius.provider.js.map