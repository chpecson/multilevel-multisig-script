"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var sirius_provider_1 = require("../providers/sirius.provider");
var config = __importStar(require("../../config.json"));
var SignersGenerator = /** @class */ (function () {
    function SignersGenerator(listener) {
        this.approvalTree = {};
        this.staffCount = 5;
        this.autosignCount = 2;
        this.directorCount = 2;
        this.managerCount = 2;
        this.interimCount = config.counts.interim;
        this.siriusProvider = new sirius_provider_1.SiriusProvider(listener);
    }
    SignersGenerator.prototype.generateReverseXTopology = function (interimAAccounts, interimBAccounts) {
        if (interimAAccounts === void 0) { interimAAccounts = []; }
        if (interimBAccounts === void 0) { interimBAccounts = []; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                console.log('Start Generating Interims ', new Date().toLocaleTimeString());
                return [2 /*return*/, Promise.all([
                        this.generateInterimNodes(interimAAccounts),
                        this.generateInterimNodes(interimBAccounts),
                    ]).then(function (interims) {
                        console.log('Done Generating Interims ', new Date().toLocaleTimeString());
                        console.log('----------------------------');
                        console.log('Start Generating Signer Parents ', new Date().toLocaleTimeString());
                        return Promise.all([
                            _this.siriusProvider.generateSignerNode(interims[0].provider, 2, 1, 1),
                            _this.siriusProvider.generateSignerNode(interims[1].provider, 2, 2, 2),
                        ]).then(function (providers) {
                            return { providers: providers, interims: interims };
                        });
                    }).then(function (response) {
                        var providers = response.providers;
                        console.log('Done Generating Signer Parents ', new Date().toLocaleTimeString());
                        console.log('----------------------------');
                        console.log('start Generating Signers ', new Date().toLocaleTimeString());
                        return Promise.all([
                            _this.siriusProvider.generateSignerNode(providers[0].signerParents[0], 2, 2, 2),
                            _this.siriusProvider.generateSignerNode(providers[0].signerParents[1], 2, 1, 1),
                            _this.siriusProvider.generateSignerNode(providers[1].signerParents[0], 2, 1, 1),
                            _this.siriusProvider.generateSignerNode(providers[1].signerParents[1], 5, 1, 1),
                        ]).then(function (signers) {
                            console.log('Done Generating Signers ', new Date().toLocaleTimeString());
                            console.log('----------------------------');
                            console.log('start Generating autoSign parents ', new Date().toLocaleTimeString());
                            return Promise.all(signers[0].signerParents.map(function (signer) { return _this.siriusProvider.generateSignerNode(signer, 2, 1, 1); })).then(function (autoSigners) {
                                return _this.mapResult(response, signers, autoSigners);
                            }).catch(function (error) { return console.log(error); });
                        });
                    })];
            });
        });
    };
    SignersGenerator.prototype.mapResult = function (response, signers, autoSigners) {
        var _this = this;
        return {
            providerA: {
                account: this.siriusProvider.convertToKeys(response.interims[0].provider),
                interimNodes: response.interims[0].interimNodes.map(function (interimNode) { return _this.siriusProvider.convertToKeys(interimNode); }),
                signerNodes: {
                    autoSign: {
                        account: this.siriusProvider.convertToKeys(signers[0].provider),
                        signerParents: signers[0].signerParents.map(function (signer, index) { return ({
                            account: _this.siriusProvider.convertToKeys(signer),
                            signers: autoSigners[index].signerParents.map(function (autoSigner) { return _this.siriusProvider.convertToKeys(autoSigner); })
                        }); })
                    },
                    director: {
                        account: this.siriusProvider.convertToKeys(signers[1].provider),
                        signers: signers[1].signerParents.map(function (signer) { return _this.siriusProvider.convertToKeys(signer); })
                    }
                }
            },
            providerB: {
                account: this.siriusProvider.convertToKeys(response.interims[1].provider),
                interimNodes: response.interims[1].interimNodes.map(function (interimNode) { return _this.siriusProvider.convertToKeys(interimNode); }),
                signerNodes: {
                    manager: {
                        account: this.siriusProvider.convertToKeys(signers[2].provider),
                        signers: signers[2].signerParents.map(function (signer) { return _this.siriusProvider.convertToKeys(signer); })
                    },
                    staff: {
                        account: this.siriusProvider.convertToKeys(signers[3].provider),
                        signers: signers[3].signerParents.map(function (signer) { return _this.siriusProvider.convertToKeys(signer); })
                    }
                }
            }
        };
    };
    SignersGenerator.prototype.generateInterimNodes = function (interimAccounts) {
        return __awaiter(this, void 0, void 0, function () {
            var provider, interimNodes;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        provider = this.siriusProvider.generateAccount();
                        interimNodes = new Array(this.interimCount).fill(null).map(function (item) { return _this.siriusProvider.generateAccount(); });
                        interimAccounts.forEach(function (interim) { return interimNodes.push(interim); });
                        return [4 /*yield*/, Promise.all(interimNodes.map(function (interimNode) {
                                return _this.siriusProvider.convertToMultiSignAccount(interimNode, 1, 1, [provider]);
                            }))];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, Promise.resolve({ interimNodes: interimNodes, provider: provider })];
                }
            });
        });
    };
    return SignersGenerator;
}());
exports.SignersGenerator = SignersGenerator;
//# sourceMappingURL=signers.js.map