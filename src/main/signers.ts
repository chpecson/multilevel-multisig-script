import {Account, Listener, MultisigAccountInfo, NetworkType, SimpleWallet} from "tsjs-xpx-chain-sdk";
import {SiriusProvider} from "../providers/sirius.provider";
import * as config from '../../config.json';

export class SignersGenerator {

    private autoSignParent: MultisigAccountInfo;
    private siriusProvider: SiriusProvider;
    private approvalTree: any = {};
    private staffCount = 5;
    private autosignCount = 2;
    private directorCount = 2;
    private managerCount = 2;
    private interimCount = config.counts.interim;

    constructor(listener: Listener) {
        this.siriusProvider = new SiriusProvider(listener);
    }
    async generateReverseXTopology(interimAAccounts: Account[] = [], interimBAccounts: Account[] = []) {
        console.log('Start Generating Interims ', new Date().toLocaleTimeString());
        return Promise.all([
            this.generateInterimNodes(interimAAccounts), // Interim A
            this.generateInterimNodes(interimBAccounts), // Interim B
        ]).then((interims) => {
            console.log('Done Generating Interims ', new Date().toLocaleTimeString());
            console.log('----------------------------');
            console.log('Start Generating Signer Parents ', new Date().toLocaleTimeString());
            return Promise.all(
                [
                    this.siriusProvider.generateSignerNode(interims[0].provider, 2,1,1),
                    this.siriusProvider.generateSignerNode(interims[1].provider, 2, 2,2),
                ]
            ).then((providers) => {
                return { providers, interims }
            })
        }).then((response) => {
            const providers = response.providers;
            console.log('Done Generating Signer Parents ', new Date().toLocaleTimeString());
            console.log('----------------------------');
            console.log('start Generating Signers ', new Date().toLocaleTimeString());
            return Promise.all([
                this.siriusProvider.generateSignerNode(providers[0].signerParents[0], 2,2,2), // Autosigns
                this.siriusProvider.generateSignerNode(providers[0].signerParents[1], 2,1,1), // Directors
                this.siriusProvider.generateSignerNode(providers[1].signerParents[0], 2,1,1), // Manager
                this.siriusProvider.generateSignerNode(providers[1].signerParents[1], 5,1,1), // Staff
            ]).then( (signers) => {
                console.log('Done Generating Signers ', new Date().toLocaleTimeString());
                console.log('----------------------------');
                console.log('start Generating autoSign parents ', new Date().toLocaleTimeString());
                return Promise.all(signers[0].signerParents.map(signer => this.siriusProvider.generateSignerNode(signer, 2,1,1))).then(autoSigners => {
                    return this.mapResult(response,signers, autoSigners);
                }).catch(error => console.log(error))
            })
        });
    }

    private mapResult(response, signers, autoSigners) {
        return {
            providerA: {
                account: this.siriusProvider.convertToKeys(response.interims[0].provider),
                    interimNodes: response.interims[0].interimNodes.map(interimNode => this.siriusProvider.convertToKeys(interimNode)),
                    signerNodes: {
                    autoSign: {
                        account: this.siriusProvider.convertToKeys(signers[0].provider),
                        signerParents: signers[0].signerParents.map((signer, index) => ({
                            account: this.siriusProvider.convertToKeys(signer),
                            signers: autoSigners[index].signerParents.map(autoSigner => this.siriusProvider.convertToKeys(autoSigner))
                        }))
                    },
                    director: {
                        account: this.siriusProvider.convertToKeys(signers[1].provider),
                        signers: signers[1].signerParents.map(signer => this.siriusProvider.convertToKeys(signer))
                    }
                }
            },
            providerB: {
                account: this.siriusProvider.convertToKeys(response.interims[1].provider),
                    interimNodes: response.interims[1].interimNodes.map(interimNode => this.siriusProvider.convertToKeys(interimNode)),
                    signerNodes: {
                    manager: {
                        account: this.siriusProvider.convertToKeys(signers[2].provider),
                        signers: signers[2].signerParents.map(signer => this.siriusProvider.convertToKeys(signer))
                    },
                    staff: {
                        account: this.siriusProvider.convertToKeys(signers[3].provider),
                        signers: signers[3].signerParents.map(signer => this.siriusProvider.convertToKeys(signer))
                    }
                }
            }
        };
    }

    private async generateInterimNodes(interimAccounts: Account[]) {
        const provider = this.siriusProvider.generateAccount();
        const interimNodes = new Array(this.interimCount).fill(null).map(item => this.siriusProvider.generateAccount());
        interimAccounts.forEach(interim => interimNodes.push(interim));
        await Promise.all(interimNodes.map(interimNode =>
            this.siriusProvider.convertToMultiSignAccount(interimNode, 1, 1, [provider]))
        );
        return Promise.resolve({ interimNodes, provider });
    }
    
}
