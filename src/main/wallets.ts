import {SignersGenerator} from "./signers";
import {Account, Listener, KeyPair} from "tsjs-xpx-chain-sdk";
import * as fs from 'fs';
import {SiriusProvider} from "../providers/sirius.provider";


export class WalletsGenerator {
    private signersGenerator: SignersGenerator;
    siriusProvider: SiriusProvider;
    interimNodes = [];

    constructor(listener: Listener) {
        this.siriusProvider = new SiriusProvider(listener);
        this.interimNodes = new Array(2).fill(null).map(item => this.siriusProvider.generateAccount());
    }

    async generateWallets(walletsNeeded: string[]): Promise<any> {
        const wallets = new Array(walletsNeeded.length).fill(null).map(item => this.generateWallet().catch(() => null));
        return Promise.all(wallets).then((args) => ({
            interimNodes: this.interimNodes,
            wallets: walletsNeeded.map((walletNeeded, index) => ({
                name: walletNeeded,
                walletSet: args[index]
            }))
        })).catch(() => null);
    }
    
    private async generateWallet() {
        const msuAccount = this.siriusProvider.generateAccount();
        const msx = this.siriusProvider.generateAccount();
        await this.siriusProvider.convertToMultiSignAccount(msx, 2,2, [msuAccount, ...this.interimNodes]);
        const msu = await this.siriusProvider.generateSignerNode(msuAccount, 1,1,1);
        const mainSigner = msu.signerParents[0];
        return Promise.resolve({ mainSigner: this.siriusProvider.convertToKeys(mainSigner), msu: this.siriusProvider.convertToKeys(msuAccount), msx: this.siriusProvider.convertToKeys(msx)});
    }

    public async generateMetadataNodes(metaDataWallets: string[]) {
        const msuAccount = this.siriusProvider.generateAccount();
        const msx = this.siriusProvider.generateAccount();
        const ic = this.siriusProvider.generateAccount();
        const metaDataWalletArray = new Array(metaDataWallets.length).fill(null).map(item => this.siriusProvider.generateAccount());
        console.log('convertToMultiSignAccount -> msx -> ', msx.publicAccount.publicKey);
        await this.siriusProvider.convertToMultiSignAccount(msx, 2,2, [ic, ...this.interimNodes]).catch(() => null);
        console.log('convertToMultiSignAccount -> ic -> ', ic.publicAccount.publicKey);
        await this.siriusProvider.convertToMultiSignAccount(ic, 1,1, metaDataWalletArray).catch(() => null);

        await Promise.all(metaDataWalletArray.map(item => this.siriusProvider.convertToMultiSignAccount(item, 1,1,[msuAccount])));

        console.log('generateSignerNode -> msuAccount -> ', msuAccount.publicAccount.publicKey);
        const msu = await this.siriusProvider.generateSignerNode(msuAccount, 5,1,2).catch(() => null);
        const mainSigner = msu.signerParents[0];
        return Promise.resolve({
            signers: msu.signerParents.map(signer => this.siriusProvider.convertToKeys(signer)),
            wallets: metaDataWalletArray.map((wallet, index) => ({
                account: this.siriusProvider.convertToKeys(wallet),
                name: metaDataWallets[index]
            })),
            msu: this.siriusProvider.convertToKeys(msuAccount),
            msx: this.siriusProvider.convertToKeys(msx),
            ic: this.siriusProvider.convertToKeys(ic)
        })
    }
}
