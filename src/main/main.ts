import {ProviderGenerator} from "./providers";
import {Listener} from "tsjs-xpx-chain-sdk";
import * as config from "../../config.json";

const listener = new Listener(config.wsNodeUrl);
const bootstrap = async () => {

    listener.open().then(async () => {
        console.log('Listener Opened');
        const generator = new ProviderGenerator(listener);
        await generator.start();
        process.exit();
        console.log('closing listener');
        listener.close();
    });
};

bootstrap().then(() => {
    console.log('bootstrap started');
});

process.on('SIGINT', (code) => {
    console.log('closing listener');
    listener.close();
    console.log('status => ', listener.isOpen());
});


