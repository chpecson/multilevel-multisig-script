import {SignersGenerator} from "./signers";
import {Listener} from "tsjs-xpx-chain-sdk";
import * as fs from 'fs';
import {WalletsGenerator} from "./wallets";
import * as config from '../../config.json';


export class ProviderGenerator {
    private signersGenerator: SignersGenerator;
    private walletsGenerator: WalletsGenerator;

    constructor(listener: Listener) {
        this.signersGenerator = new SignersGenerator(listener);
        this.walletsGenerator = new WalletsGenerator(listener);
    }

    async start(): Promise<void> {
        console.log('----------------------------');
        console.log('start');
        console.log('Generating System Wallets ', new Date().toLocaleTimeString());//,, 'wss', 'fd'
        const metaDataWallets = await this.walletsGenerator.generateMetadataNodes(config.counts.metaDataWallets);
        console.log('Done Generating Metadata Wallets ', new Date().toLocaleTimeString());
        console.log('----------------------------');
        console.log('Generating System Wallets ', new Date().toLocaleTimeString());//,, 'wss', 'fd'
        const wallets = await this.walletsGenerator.generateWallets(config.counts.wallets).catch(() => null);
        console.log('Done Generating System Wallets ', new Date().toLocaleTimeString());
        console.log('----------------------------');
        console.log('Generating Signers ', new Date().toLocaleTimeString());//,, 'wss', 'fd'
        const signers = await this.signersGenerator.generateReverseXTopology([this.walletsGenerator.interimNodes[0]], [this.walletsGenerator.interimNodes[1]]).catch(() => null);
        console.log('Done Generating Signers ', new Date().toLocaleTimeString());
        console.log('----------------------------');
        console.log('signers', signers);
        signers['wallets'] = wallets.wallets;
        signers['metaDataWallets'] = metaDataWallets;
        return new Promise<void>((resolve) => {
            fs.appendFile(Date.now() + '.json', JSON.stringify(signers), function (err) {
                if (err) throw err;
                console.log('Saved!');
                resolve();
            });
        })
    }
}
