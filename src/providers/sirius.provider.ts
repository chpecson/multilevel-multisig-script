import {
    Account,
    Address,
    AggregateTransaction,
    CosignatureTransaction,
    Deadline,
    HashLockTransaction,
    Listener,
    ModifyMultisigAccountTransaction,
    Mosaic,
    MosaicId,
    MultisigCosignatoryModification,
    MultisigCosignatoryModificationType,
    NetworkType,
    Transaction,
    TransactionHttp,
    UInt64,
} from 'tsjs-xpx-chain-sdk';

import * as config from '../../config.json';

export class SiriusProvider {
    networkType = NetworkType[config.networkType];
    private transactionHttp: TransactionHttp;

    constructor(public listener: Listener) {
        this.transactionHttp = new TransactionHttp(config.httpNodeURL);
    }

    generateAccount(): Account {
        return Account.generateNewAccount(this.networkType);
    }

    async convertToMultiSignAccount(toConvert: Account,minApproval: number, minRemoval, accounts: Account[]): Promise<any> {

        const cosigners = accounts.map(cosigner => new MultisigCosignatoryModification(MultisigCosignatoryModificationType.Add, cosigner.publicAccount));
        const convertIntoMultisigAccount = ModifyMultisigAccountTransaction.create(
            Deadline.create(),
            minApproval,
            minRemoval,
            cosigners,
            this.networkType);

        const aggregateTransaction = AggregateTransaction.createBonded(
            Deadline.create(),
            [convertIntoMultisigAccount.toAggregate(toConvert.publicAccount)],
            this.networkType);

        const signedTransaction = aggregateTransaction.signWith(toConvert, config.generationHash);

        const hashLockTransaction = HashLockTransaction.create(Deadline.create(),
            new Mosaic(new MosaicId('bffb42a19116bdf6'), UInt64.fromUint(0)),
            UInt64.fromUint(480),
            signedTransaction,
            this.networkType);

        const hashLockTransactionSigned = hashLockTransaction.signWith(toConvert, config.generationHash);

        this.transactionHttp.announce(hashLockTransactionSigned).toPromise().then((response) => {
            console.log(response);
        }).catch(err => console.log(err));
        console.log(`58 - transactionHttp.announce(hashLockTransactionSigned) by ${toConvert.publicAccount.publicKey} -> ${hashLockTransactionSigned.hash}`);

        return this.createConfirmedListenerPromise(toConvert.address,'77', async (resolve, transaction: any, subscription) => {
            console.log(`transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash -> `, transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash);
            if (transaction !== undefined && transaction.transactionInfo.hash === hashLockTransactionSigned.hash) {
                this.transactionHttp.announceAggregateBonded(signedTransaction).toPromise();
                console.log(`63 - this.transactionHttp.announceAggregateBonded(signedTransaction) -> ${signedTransaction.hash}`);
                resolve(
                    this.createPartialListenerPromise(toConvert.address, '83', async (partialResolve, partialTransaction, partialSubscription) => {
                        if (partialTransaction !== undefined && partialTransaction.transactionInfo.hash === signedTransaction.hash) {
                            console.log(`68 - CosignatureTransaction.create(partialTransaction) -> ${partialTransaction.transactionInfo.hash}`);
                            const toSignCosignTransaction = CosignatureTransaction.create(partialTransaction);
                            const cosignPromise = await Promise.all(
                                     accounts.map(async account => { // simple wallet signing
                                        const signedCosignedTransaction = account.signCosignatureTransaction(toSignCosignTransaction);
                                        console.log(`73 - this.transactionHttp.announceAggregateBondedCosignature(signedCosignedTransaction) -> ${account.publicAccount.publicKey} -> ${partialTransaction.transactionInfo.hash}`);
                                        this.transactionHttp.announceAggregateBondedCosignature(signedCosignedTransaction);
                                        return this.createConfirmedListenerPromise(account.address, '105',(cosignResolve, cosignedTransaction: Transaction, cosignSubscription) => {
                                            console.log(`76 - cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash ->`, cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash);
                                            if (cosignedTransaction !== undefined && cosignedTransaction.transactionInfo.hash === signedCosignedTransaction.parentHash) {
                                                console.log(`78 -> cosignSubscription.unsubscribe() -> ${cosignedTransaction.transactionInfo.hash}`);
                                                cosignSubscription.unsubscribe();
                                                cosignResolve(true);
                                            }
                                        })
                                    })
                            ).then((args) => {
                                console.log(args);
                                console.log(`84 - partialResolve(cosignPromise)`);
                                partialSubscription.unsubscribe();
                                partialResolve();
                                subscription.unsubscribe();
                            }).catch(() => null);
                        }
                    })
                );
            }
        })
    }

    private createConfirmedListenerPromise(address: Address, type: string, callBack: Function): Promise<any> {
        return new Promise((resolve) => {
            const subscription = this.listener.confirmed(address).subscribe((transaction: any) => {
                callBack(resolve, transaction, subscription)
            });
        }).catch(() => null);
    }

    private createPartialListenerPromise(address: Address, type: string, callBack: Function): Promise<any> {
        return new Promise((resolve) => {
            const subscription = this.listener.aggregateBondedAdded(address).subscribe((transaction: any) => {
                callBack(resolve, transaction, subscription)
            });
        }).catch(() => null);
    }

    public async generateSignerNode(provider: Account, signerCount: number, minApproval: number, minRemoval: number) {
        const signerParents = new Array(signerCount).fill(null).map(item => this.generateAccount());
        await this.convertToMultiSignAccount(provider, minApproval, minRemoval, signerParents).catch(() => null);
        return Promise.resolve({ provider, signerParents });
    }

    public convertToKeys(account: Account) {
        return {
            address: account.address.plain(),
            publicKey: account.publicAccount.publicKey,
            privateKey: account.privateKey,
        }
    }
}
