# interim multisign creator
Script to generate multi level multi sign structure

## Generate interim multisign
`npm start`

## Generate interim multisign and interim keys
`npm run generate:providers`

## Generate interimA level 1, interimB level 1
`npm run generate:interims`